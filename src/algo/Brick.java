package algo;

public class Brick {
	
	private char orientation;
	private int value;
	private int[] coordsFirstHalf;
	private int[] coordsSecondHalf;
	
	public char getOrientation() {
		return orientation;
	}

	public void setOrientation(char orientation) {
		this.orientation = orientation;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Brick(char orientation, int value, int[] coordsFirstHalf, int[] coordsSecondHalf) {
		this.orientation = orientation;
		this.value = value;
		this.coordsFirstHalf = coordsFirstHalf;
		this.coordsSecondHalf = coordsSecondHalf;
	}

	public int[] getcoordsFirstHalf() {
		return coordsFirstHalf;
	}

	public int[] getcoordsSecondHalf() {
		return coordsSecondHalf;
	}

}
