package algo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

/**
 *  first read input in array named layer. After that iterate over array in convert every brick to instance of class Brick and put it in Queue.
 *  Next read from queue and make decision for element on his orientation.
 * 	this is maked for every four elements in queue;  
 * 
 * possible variants:
 * 
 * HHVV -> VVHH
 * HHHH -> VHHV
 * HHVH -> not possible to exist independently, special case   
 * HHHV -> not possible to exist independently , special case
 * 
 * VVHH -> HHVV
 * VVVV -> HHHH
 * VHHH -> not possible to exist independently, special case
 * 
 * 
 * */

public class BrickWall {
		
	public static void main(String[] args) throws IOException {
		// width of layer
		var width = 0;
		//height of layer
		var height = 0;
		// input brick layer
		int[][] layer;
		// next read inputs and parse it -> finally we have layer[height][width]
		List<List<String>> lines = new ArrayList<>();
		Scanner scanner = new Scanner(System.in);
		String line;
		while(scanner.hasNextLine()) {
			line = scanner.nextLine();
			if(line.isEmpty()) {
				break;
			}
			var currentLine = new ArrayList<String>();
			currentLine.add(line);
			lines.add(currentLine);
		}
		var dim = lines.get(0).get(0 ).split(" ");
		height = Integer.parseInt(dim[0]);
		width = Integer.parseInt(dim[1]);
		if(width % 2 != 0 || height % 2 != 0 || width > 100 || height > 100) 
			throw new IllegalArgumentException(" -1 - Wrong input");
		layer = new int[height][width];
		lines.remove(0);
		int i=0;
		while(!lines.isEmpty()) {
			var row = lines.get(0).get(0).split(" ");
			for(int j=0; j < width; j++) {
				layer[i][j] = Integer.parseInt(row[j]);
			}
			i++;
			lines.remove(0);
		}
		processInputLayer(layer, height, width);
	}
	
	// making queue with all bricks
	public static void processInputLayer(int[][] layer, int height, int width) {
		Queue<Brick> elementQueue = new LinkedList<Brick>();
		for(int row = 0; row <= height-1; row+=2) {
			for(int col = 0; col <= width-1; col++) {
				if(layer[row][col] == layer[row+1][col]) {
					//vertical
					System.out.println('V');
					int[] x = {row, col};
					int[] y = {row+1, col};
					var brick = new Brick('V', layer[row][col], x, y);
					elementQueue.add(brick);
				} else if(layer[row][col] == layer[row][col+1]) {
					//horizontal
					System.out.println('H');
					int[] x = {row, col};
					int[] y = {row, col+1};
					var brick = new Brick('H', layer[row][col], x, y);
					elementQueue.add(brick);
					int[] x1 = {row+1, col};
					int[] y1 = {row+1, col+1};
					brick = new Brick('H', layer[row+1][col],x1,y1);
					elementQueue.add(brick);
					System.out.println('H');
					col++;
				}
			}
		}
		createNewLayer(elementQueue, height, width);
	}
	
	// create new layer of wall
	public static void createNewLayer(Queue<Brick> bricks, int height, int width) {
		int[][] newLayer = new int[height][width];
		boolean beforeBrickvertical = false;
		boolean beforeBrickhorizontal = false;
		boolean oddRow = false;
		boolean sc = false;
			Brick beforeBrick = null;
			while(!bricks.isEmpty()) {
				Brick brick = bricks.poll();
				if (brick.getOrientation() == 'V') {
					if(!beforeBrickvertical && !beforeBrickvertical) {
						// make vertical brick horizontal
						newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
						newLayer[brick.getcoordsSecondHalf()[0] -1][brick.getcoordsSecondHalf()[1] +1] = brick.getValue();
						brick = bricks.poll();
						if(brick.getOrientation() == 'V') {
							// make second vertical brick horizontal
							newLayer[brick.getcoordsFirstHalf()[0] +1][brick.getcoordsFirstHalf()[1] -1] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
							if(!bricks.isEmpty() && bricks.element().getOrientation() == 'H') {
								//make third brick vertical
								brick = bricks.poll();
								newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
								newLayer[brick.getcoordsSecondHalf()[0] +1][brick.getcoordsSecondHalf()[1] -1] = brick.getValue();
								// make four brick vertical
								brick = bricks.poll();
								newLayer[brick.getcoordsFirstHalf()[0] -1][brick.getcoordsFirstHalf()[1] +1] = brick.getValue();
								newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
							}
						}else{
							// stay second brick horizontal
							newLayer[brick.getcoordsFirstHalf()[0] +1][brick.getcoordsFirstHalf()[1] -1] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0] +1][brick.getcoordsSecondHalf()[1]-1] = brick.getValue();
							//stay third brick horizontal
							brick = bricks.poll();
							newLayer[brick.getcoordsFirstHalf()[0] -1][brick.getcoordsFirstHalf()[1] +1] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0] -1][brick.getcoordsSecondHalf()[1] +1] = brick.getValue();
							brick = bricks.poll();
							if(brick.getOrientation() == 'V') {
								//make four brick horizontal
								newLayer[brick.getcoordsFirstHalf()[0] +1][brick.getcoordsFirstHalf()[1] - 1] = brick.getValue();
								newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
							} else {
								//stay four brick horizontal
								newLayer[brick.getcoordsFirstHalf()[0] +1][brick.getcoordsFirstHalf()[1] - 1] = brick.getValue();
								newLayer[brick.getcoordsSecondHalf()[0] +1][brick.getcoordsSecondHalf()[1] -1] = brick.getValue();
								sc = true;
							}
						}
					} 
					brick = null;
				}
				if (brick != null && brick.getOrientation() == 'H') {
					if(!beforeBrickvertical && !beforeBrickhorizontal) {
						if(!oddRow && !sc) {
							//make first brick vertical
							newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0] +1][brick.getcoordsSecondHalf()[1] -1] = brick.getValue();
							//stay second brick horizontal
							brick = bricks.poll();
							newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1] + 1] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1] + 1] = brick.getValue();
						} else {
							//make first brick vertical 
							newLayer[brick.getcoordsFirstHalf()[0] -1][brick.getcoordsFirstHalf()[1] +1] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
							// make second brick vertical
							brick = bricks.poll();
							newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
							newLayer[brick.getcoordsSecondHalf()[0] +1][brick.getcoordsSecondHalf()[1] -1] = brick.getValue();
							sc = false;
						}
							if(bricks.element().getOrientation() == 'H') {
								brick = bricks.poll();
								if(bricks.element().getOrientation() == 'V') {
									//stay third brick horizontal
									newLayer[brick.getcoordsFirstHalf()[0] -1][brick.getcoordsFirstHalf()[1] +1] = brick.getValue();
									newLayer[brick.getcoordsSecondHalf()[0] -1][brick.getcoordsSecondHalf()[1] +1] = brick.getValue();
									brick = bricks.poll();
									//stay four brick horizontal
									newLayer[brick.getcoordsFirstHalf()[0] +1][brick.getcoordsFirstHalf()[1] -1] = brick.getValue();
									newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
								} else {
									//stay third brick horizontal
									newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]-1] = brick.getValue();
									newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]-1] = brick.getValue();
									brick = bricks.poll();
									beforeBrickhorizontal = true;
									if(!bricks.isEmpty() && beforeBrickhorizontal) {
										//make four brick vertical
										newLayer[brick.getcoordsFirstHalf()[0] -1][brick.getcoordsFirstHalf()[1] +1] = brick.getValue();
										newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
									} else {
										//brick not rotate
										newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
										newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
									}
								}	
							} else {
								//make third brick horizontal
								brick = bricks.poll();
									newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1] -1] = brick.getValue();
									newLayer[brick.getcoordsSecondHalf()[0] -1][brick.getcoordsSecondHalf()[1]] = brick.getValue();
									brick = bricks.poll();
								if(bricks.element().getOrientation() == 'V') {
									//brick not rotate
									newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
									newLayer[brick.getcoordsSecondHalf()[0]][brick.getcoordsSecondHalf()[1]] = brick.getValue();
								}else {
									//make four brick vertical
									newLayer[brick.getcoordsFirstHalf()[0]][brick.getcoordsFirstHalf()[1]] = brick.getValue();
									newLayer[brick.getcoordsSecondHalf()[0] +1][brick.getcoordsSecondHalf()[1] -1] = brick.getValue();
									oddRow = true;
								}
							}	
						}
					}
				}
		printLayer(newLayer, height, width);
		}
	
	// printing new layer 
	public static void printLayer(int[][] newLayer, int height, int width) {
		for(int i=0; i< height; i++) {
			System.out.print("|");
			for(int j=0; j<width-1; j++) {
				System.out.print(newLayer[i][j]);
				if(newLayer[i][j] != newLayer[i][j+1]) {
					System.out.print("|");
				}else if(j+1 == width-1) {
					if(Integer.toString(newLayer[i][j]).matches("^[1-9]")) {
						System.out.print(" ");
					}
					System.out.print("*");
					if(Integer.toString(newLayer[i][j]).matches("^[1-9]")) {
						System.out.print(" ");
					}
					System.out.print(newLayer[i][j]);
				} else if(newLayer[i][j] == newLayer[i][j+1]) {
					if(Integer.toString(newLayer[i][j]).matches("^[1-9]")) {
						System.out.print(" ");
					}
					System.out.print("*");
					if(Integer.toString(newLayer[i][j]).matches("^[1-9]")) {
						System.out.print(" ");
					}	
				}
			}
			System.out.print("|");
			System.out.println();
		}
	}
}